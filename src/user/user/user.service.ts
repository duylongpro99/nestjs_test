import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { UpdateResult, DeleteResult } from "typeorm";

@Injectable()
export class UserService {
    constructor(@InjectRepository(User) private userRepository: Repository<User>) {
        
    }
    //Create
    async Create(user: User): Promise<User>{
        return await this.userRepository.save(user);
    }
    //Read
    async Read(): Promise<User[]>{
        return await this.userRepository.find();
    }
    //Update
    async Update(user: User): Promise<UpdateResult>{
        return await this.userRepository.update(user.id, user);
    }
    //Delete
    async Delete(id):Promise<DeleteResult>{
        return await this.userRepository.delete(id);
    }
}
