import { UserService } from './user.service';
import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { User } from './user.entity';


@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}
  //localhost:4000/user
  @Get()
  listUsers(): Promise<User[]> {
    return this.userService.Read();
  }

  // /user/create
  @Post('create')
  createUser(@Body() newUser: User): Promise<User> {
    return this.userService.Create(newUser);
  }

  // /user/update/:id
  @Put('update/:id')
  async UpdateUser(
    @Param('id') id: string,
    @Body() updatedUser: User
  ): Promise<any> {
    updatedUser.id = Number(id);
    return this.userService.Update(updatedUser);
  }

  // /user/delete/:id
  @Delete('delete/:id')
  async DeleteUser(@Param('id') id: string): Promise<any> {
    return this.userService.Delete(id);
  }
}
