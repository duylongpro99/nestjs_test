import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const config: TypeOrmModuleOptions = {
    username:  'postgres',
    password: 'postgres',
    type : 'postgres',
    port : 3011,
    host : 'localhost',
    database: 'demo',
    synchronize: false,
    logging: true,
    entities: ["dist/**/*.entity{.ts,.js}"]

} 